# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

AntiKt4LCTopoJetsCPContent = [
"Kt4LCTopoOriginEventShape",
"Kt4LCTopoOriginEventShapeAux.Density",
"AntiKt4LCTopoJets",
"AntiKt4LCTopoJetsAux.pt.eta.phi.m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.NumTrkPt500.SumPtTrkPt500.EnergyPerSampling.EMFrac.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.DetectorEta.FracSamplingMax.FracSamplingMaxIndex.GhostTrack.HECFrac.HECQuality.JetPileupScaleMomentum_eta.JetPileupScaleMomentum_m.JetPileupScaleMomentum_phi.JetPileupScaleMomentum_pt.Jvt.JVFCorr.JvtRpt.LArQuality.NegativeE.NumTrkPt1000.OriginCorrected.PileupCorrected.TrackWidthPt1000.GhostMuonSegment.GhostMuonSegmentCount.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_m.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_pt",
"MET_Track",
"MET_TrackAux.name.mpx.mpy",
"MuonSegments",
"MuonSegmentsAux.",
"PrimaryVertices",
"PrimaryVerticesAux.vertexType"
]
